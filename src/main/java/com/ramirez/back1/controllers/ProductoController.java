package com.ramirez.back1.controllers;

import com.ramirez.back1.models.ProductoModel;
import com.ramirez.back1.models.ProductoPrecioOnly;
import com.ramirez.back1.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/apitechu/v1")
public class ProductoController {

    @Autowired
    private ProductoService productService;

    @GetMapping("")
    public String root() {
        return "Techu API REST v1.0.0";
    }

    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel productoModel) {
        productService.addProducto(productoModel);
        return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody ProductoModel productToUpdate) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.updateProducto(id-1, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK); // Buenas prácticas dicen que mejor enviar 204 No Content
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        ProductoModel pr = productService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.removeProducto(id - 1);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // status code 204-No Content
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoPrecioOnly productoPrecioOnly, @PathVariable int id){
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productService.updateProducto(id-1, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null)
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
